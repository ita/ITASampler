/*
 *      +------------------+
 *  --->| ITA Sampler      |--->
 *  --->| o o o o      /// |--->
 *      +------------------+
 *
 *  ITASampler ist eine C++ Bibliothek die einen Sound sampler zur wavetable-basierten
 *  Klangsynthese bereitstellt. Der Begriff "sampler" bezieht sich hier auf den Terminus
 *  aus der Musikproduktion und nicht auf Abtastwerte.
 *
 *  Datei:		ITASoundSamplerImpl.cpp
 *  Zweck:		Implementierungsklasse des sound samplers
 *  Autoren:	Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *  CVS-Datum:	$Id: ITASoundSamplerImpl.cpp,v 1.3 2010-04-19 14:24:57 fwefers Exp $
 *
 *  (c) Copyright Institut f�r Technische Akustik (ITA) RWTH Aachen, 2008
 */

#include "ITASoundSamplerImpl.h"

#include "ITASoundSampleImpl.h"
#include "ITASoundSamplePoolImpl.h"
#include "ITASoundSamplerPlayback.h"
#include "PlaybackIDGenerator.h"

#include <ITABaseDefinitions.h>
#include <ITAException.h>
#include <ITAFade.h>
#include <ITAFunctors.h>
#include <ITANumericUtils.h>
#include <ITASampleClock.h>
#include <ITAStringUtils.h>
#include <algorithm>
#include <cstring>

// Vorgabe: Anzahl Samples f�r Fading bei Pause/Fortsetzen einer Wiedergabe
#define DEFAULT_PAUSE_FADE_LENGTH 256

ITASoundSamplerImpl::ITASoundSamplerImpl( int iOutputChannels, double dSamplerate, int iBlocklength, ITASoundSamplePool* pSamplePool )
    : ITADatasourceRealization( (unsigned)iOutputChannels, dSamplerate, (unsigned)iBlocklength, 2 )
    , m_pDefaultSampleClock( NULL )
    , m_pDefaultSamplePool( NULL )
{
	m_iTrackIDCount = 0;

	m_iCurrentBlockIndex = 0;
	m_bProcess           = true;
	m_dMasterGain        = 1.0;

	// Standard Sampleclock = 1s Raster
	m_pDefaultSampleClock = new ITASampleClock( ITADatasourceRealization::m_dSampleRate, 1.0 );
	m_pSampleClock        = m_pDefaultSampleClock;

	if( pSamplePool )
	{
		// Benutzung eines fremden Samplepools
		m_pSamplePool = dynamic_cast<ITASoundSamplePoolImpl*>( pSamplePool );
	}
	else
	{
		// Eigen-verwalteter Samplepool
		m_pDefaultSamplePool = new ITASoundSamplePoolImpl( iOutputChannels, dSamplerate );
		m_pSamplePool        = m_pDefaultSamplePool;
	}
}

ITASoundSamplerImpl::~ITASoundSamplerImpl( )
{
	// Alle Playbacks entfernen
	RemoveAllPlaybacks( );

	// TODO: Resourcen freigeben
	delete m_pDefaultSampleClock;
	delete m_pDefaultSamplePool;
}

void ITASoundSamplerImpl::SetSampleClock( ITASampleClock* pClock )
{
	// TODO: Was ist wenn man das w�hrend dem Streaming aufruft?
	m_pSampleClock = pClock;
}

ITASoundSamplePool* ITASoundSamplerImpl::GetSamplePool( ) const
{
	return m_pSamplePool;
}

void ITASoundSamplerImpl::ResetSampleCount( )
{
	// TODO: Locking-Granularit�t kl�ren. Sicherheitshalber erstmal alles locken.
	// m_csSamples.enter();
	m_csTracks.enter( );
	m_csSlotbuf.enter( );

	// TODO: Clock-Resync erforderlich?
	m_iCurrentBlockIndex = 0;

	m_csSlotbuf.leave( );
	m_csTracks.leave( );
	// m_csSamples.leave();
}

int ITASoundSamplerImpl::GetSampleCount( )
{
	// TODO: Locking-Granularit�t kl�ren. Sicherheitshalber erstmal alles locken.
	// m_csSamples.enter();
	m_csTracks.enter( );
	m_csSlotbuf.enter( );

	// TODO: Clock-Resync erforderlich?
	int iResult = m_iCurrentBlockIndex * (signed)ITADatasourceRealization::m_uiBlocklength;

	m_csSlotbuf.leave( );
	m_csTracks.leave( );
	// m_csSamples.leave();

	return iResult;
}

int ITASoundSamplerImpl::AddTrack( int iTrackChannels )
{
	// Sicherheitschecks:
	if( iTrackChannels > (signed)ITADatasourceRealization::m_uiChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid number of channels" );

	m_csTracks.enter( );

	int iTrackID = m_iTrackIDCount++;
	Track track( iTrackChannels );
	m_mTracks.insert( std::pair<int, Track>( iTrackID, track ) );

	m_csTracks.leave( );

	return iTrackID;
}

void ITASoundSamplerImpl::RouteTrackChannel( int iTrackID, int iTrackChannel, int iOutputChannel )
{
	// Hinweis: Hier wird relativ grob-granular gelockt. Das ist aber nicht weiter schlimm, da
	//          Operationen auf den Spuren haupts�chlich vor dem Streaming durchgef�hrt werden.

	if( iOutputChannel >= (signed)ITADatasourceRealization::m_uiChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Output channel out of range" );

	m_csTracks.enter( );

	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
	{
		m_csTracks.leave( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );
	}

	Track& track = it->second;
	if( iTrackChannel >= track.iChannels )
	{
		m_csTracks.leave( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Track channel out of range" );
	}

	track.viChannelRoutings[iTrackChannel] = iOutputChannel;

	m_csTracks.leave( );
}

int ITASoundSamplerImpl::AddMonoTrack( int iOutputChannel )
{
	int iTrackID = AddTrack( 1 );
	RouteTrackChannel( iTrackID, 0, iOutputChannel );
	return iTrackID;
}

int ITASoundSamplerImpl::AddStereoTrack( int iLeftOutputChannel, int iRightOutputChannel )
{
	int iTrackID = AddTrack( 2 );
	RouteTrackChannel( iTrackID, 0, iLeftOutputChannel );
	RouteTrackChannel( iTrackID, 1, iRightOutputChannel );
	return iTrackID;
}

void ITASoundSamplerImpl::RemoveTrack( int iTrackID )
{
	// Hinweis: Tracks und Slotbuf locken, da ggf. Playbacks entfernt werden m�ssen
	m_csTracks.enter( );
	m_csSlotbuf.enter( );

	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
	{
		m_csSlotbuf.leave( );
		m_csTracks.leave( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );
	}

	m_mTracks.erase( it );

	// Alle Playbacks des Track l�schen
	m_sbSlotbuf.DeletePlaybacksOfTrack( iTrackID );

	m_csSlotbuf.leave( );
	m_csTracks.leave( );
}

std::string ITASoundSamplerImpl::GetTrackName( int iTrackID )
{
	m_csTracks.enter( );

	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
	{
		m_csTracks.leave( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );
	}

	Track& track      = it->second;
	std::string sName = track.sName;

	m_csTracks.leave( );
	return sName;
}

void ITASoundSamplerImpl::SetTrackName( int iTrackID, const std::string& sName )
{
	m_csTracks.enter( );

	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
	{
		m_csTracks.leave( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );
	}

	Track& track = it->second;
	track.sName  = sName;

	m_csTracks.leave( );
}

int ITASoundSamplerImpl::GetTrackNumberOfChannels( int iTrackID )
{
	m_csTracks.enter( );

	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
	{
		m_csTracks.leave( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );
	}

	Track& track  = it->second;
	int iChannels = track.iChannels;

	m_csTracks.leave( );
	return iChannels;
}

double ITASoundSamplerImpl::GetMasterGain( )
{
	// TODO: Absichern
	return m_dMasterGain;
}

void ITASoundSamplerImpl::SetMasterGain( double dGain )
{
	// TODO: Absichern
	if( dGain < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid gain" );

	m_dMasterGain = dGain;
}

double ITASoundSamplerImpl::GetTrackGain( int iTrackID )
{
	// TODO: Absichern
	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );

	Track& track = it->second;
	return track.dGain;
}

void ITASoundSamplerImpl::SetTrackGain( int iTrackID, double dGain )
{
	// TODO: Absichern
	if( dGain < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid gain" );

	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );

	Track& track = it->second;
	track.dGain  = dGain;
}

double ITASoundSamplerImpl::GetTrackChannelGain( int iTrackID, int iTrackChannel )
{
	// TODO: Absichern
	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );

	Track& track = it->second;
	if( iTrackChannel >= track.iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Track channel out of range" );

	return track.vdChannelGains[iTrackChannel];
}

void ITASoundSamplerImpl::SetTrackChannelGain( int iTrackID, int iTrackChannel, double dGain )
{
	// TODO: Absichern
	if( dGain < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid gain" );

	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );

	Track& track = it->second;
	if( iTrackChannel >= track.iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Track channel out of range" );

	track.vdChannelGains[iTrackChannel] = dGain;
}

int ITASoundSamplerImpl::CreateSample( const float* pfData, int iLength, double dSamplerate, std::string sName )
{
	// Delegat
	return m_pSamplePool->CreateSample( pfData, iLength, dSamplerate, sName );
}

int ITASoundSamplerImpl::CreateSample( const float* pfLeftChannelData, const float* pfRightChannelData, int iLength, double dSamplerate, std::string sName )
{
	// Delegat
	return m_pSamplePool->CreateSample( pfLeftChannelData, pfRightChannelData, iLength, dSamplerate, sName );
}

int ITASoundSamplerImpl::CreateSample( const float** ppfChannelData, int iNumChannels, int iLength, double dSamplerate, std::string sName )
{
	// Delegat
	return m_pSamplePool->CreateSample( ppfChannelData, iNumChannels, iLength, dSamplerate, sName );
}

int ITASoundSamplerImpl::CreateSample( std::vector<float*> vpfChannelData, int iLength, double dSamplerate, std::string sName )
{
	// Delegat
	return m_pSamplePool->CreateSample( vpfChannelData, iLength, dSamplerate, sName );
}

int ITASoundSamplerImpl::LoadSample( const std::string& sFilename, std::string sName )
{
	// Delegat
	return m_pSamplePool->LoadSample( sFilename, sName );
}

void ITASoundSamplerImpl::FreeSample( int iSampleID )
{
	// Delegat
	m_pSamplePool->FreeSample( iSampleID );
}

int ITASoundSamplerImpl::AddPlaybackBySamplecount( int iSampleID, int iTrackID, int iSamplecount, bool bSetPaused, double dGain )
{
	// Gain zuerst �berpr�fen, da am einfachsten
	if( dGain < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid gain" );

	// m_csSamples.enter();
	// if (m_mSamples.find(iSampleID) == m_mSamples.end()) {
	//	m_csSamples.leave();
	//	ITA_EXCEPT1(INVALID_PARAMETER, "Invalid sample ID");
	//}

	// Sample suchen und falls gefunden sofort (atomar) ein Playback vermerken
	const ITASoundSample* pSamplePtr = m_pSamplePool->GetSample( iSampleID, false );
	if( !pSamplePtr )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid sample ID" );

	ITASoundSampleImpl* pSample = dynamic_cast<ITASoundSampleImpl*>( const_cast<ITASoundSample*>( pSamplePtr ) );
	pSample->AddPlaybackReference( );

	m_csTracks.enter( );
	TrackMap::const_iterator trcit = m_mTracks.find( iTrackID );
	if( trcit == m_mTracks.end( ) )
	{
		m_csTracks.leave( );
		pSample->RemovePlaybackReference( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );
	}

	// Das Sample muss exakt die gleiche Anzahl Kan�le wie die Track haben
	if( pSample->GetNumberOfChannels( ) != trcit->second.iChannels )
	{
		m_csTracks.leave( );
		pSample->RemovePlaybackReference( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Sample and track number of channels do not match" );
	}

	// Timecode �berpr�fen - Muss in der Zukunft liegen
	// TODO: Hier erstmal Zwischenl�sung - �berarbeiten mit Clocks

	int iPlaybackPosition, iPlaybackBlock, iPlaybackOffset;

	if( iSamplecount > 0 )
		iPlaybackPosition = iSamplecount;
	else
		// Timecode == 0 -> Im n�chsten Block abspielen!
		iPlaybackPosition = ( m_iCurrentBlockIndex + 1 ) * (signed)ITADatasourceRealization::m_uiBlocklength - iSamplecount;

	iPlaybackBlock  = iPlaybackPosition / (signed)ITADatasourceRealization::m_uiBlocklength;
	iPlaybackOffset = iPlaybackPosition % (signed)ITADatasourceRealization::m_uiBlocklength;

	// Entweder vor Wiedergabeblock oder darin aber noch nicht Daten angefordert
	if( !( ( iPlaybackBlock > m_iCurrentBlockIndex ) || ( ( iPlaybackBlock == m_iCurrentBlockIndex ) && m_bProcess ) ) )
	{
		m_csTracks.leave( );
		pSample->RemovePlaybackReference( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid timecode - Playback must occur in the future" );
	}

	// Slot holen (kann inplizite Vergr��erung des Slotpuffers einschlie�en)
	m_csSlotbuf.enter( );
	ITASoundSamplerSlot* pSlot = m_sbSlotbuf.GetSlot( iPlaybackBlock );

	// Playback hinzuf�gen
	ITASoundSamplerPlayback* pPlayback = new ITASoundSamplerPlayback;
	pPlayback->iID                     = generatePlaybackID( );
	pPlayback->iSampleID               = iSampleID;
	pPlayback->pSample                 = pSample;
	pPlayback->iTrackID                = iTrackID;
	pPlayback->iPlaybackPosition       = iPlaybackPosition;
	pPlayback->iPlaybackBlock          = iPlaybackBlock;
	pPlayback->iPlaybackOffset         = iPlaybackOffset;
	pPlayback->iSamplePosition         = 0;
	pPlayback->dGain                   = dGain;

	pPlayback->iPauseState      = ( bSetPaused ? 2 : 0 );
	pPlayback->iPauseFadeOffset = 0;
	pPlayback->iPauseFadeLength = 0;

	m_mPlaybacks.insert( std::pair<int, ITASoundSamplerPlayback*>( pPlayback->iID, pPlayback ) );

	// Playback im Slot eintragen
	pSlot->m_vPlaybacks.push_back( pPlayback );

	m_csSlotbuf.leave( );
	m_csTracks.leave( );

	return pPlayback->iID;
}

int ITASoundSamplerImpl::AddPlaybackByTimecode( int iSampleID, int iTrackID, double dTimecode, bool bSetPaused, double dGain )
{
	if( dTimecode > 0 )
		// Samplecount zum gegebenen Timecode mittels der Sampleclock berechnen
		return AddPlaybackBySamplecount( iSampleID, iTrackID, (int)m_pSampleClock->GetSampleForTime( dTimecode ), bSetPaused, dGain );
	else
		return AddPlaybackBySamplecount( iSampleID, iTrackID, (int)0, bSetPaused, dGain );
}

void ITASoundSamplerImpl::RemovePlayback( int iPlaybackID )
{
	m_csSlotbuf.enter( );
	PlaybackMap::iterator it = m_mPlaybacks.find( iPlaybackID );
	if( it == m_mPlaybacks.end( ) )
	{
		// Playback-ID ist nicht (mehr) g�ltig
		m_csSlotbuf.leave( );
		return;
	}

	// Playback aus seinem Plot l�schen
	ITASoundSamplerPlayback* pPlayback = it->second;
	pPlayback->pSample->RemovePlaybackReference( );
	ITASoundSamplerSlot* pSlot = m_sbSlotbuf.GetSlot( pPlayback->iPlaybackBlock );
	pSlot->m_vPlaybacks.erase( std::find( pSlot->m_vPlaybacks.begin( ), pSlot->m_vPlaybacks.end( ), pPlayback ) );

	// Playback aus dem Sampler entfernen und freigeben
	m_mPlaybacks.erase( it );
	delete pPlayback;

	m_csSlotbuf.leave( );
}

void ITASoundSamplerImpl::RemovePlaybacksOfTrack( int iTrackID )
{
	m_csTracks.enter( );
	m_csSlotbuf.enter( );

	TrackMap::iterator it = m_mTracks.find( iTrackID );
	if( it == m_mTracks.end( ) )
	{
		m_csSlotbuf.leave( );
		m_csTracks.leave( );
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid track ID" );
	}

	m_sbSlotbuf.DeletePlaybacksOfTrack( iTrackID );

	m_csSlotbuf.leave( );
	m_csTracks.leave( );
}

void ITASoundSamplerImpl::RemoveAllPlaybacks( )
{
	m_csSlotbuf.enter( );

	// Den Slotbuffer vollst�ndig leeren

	// Alle Playbacks verwerfen
	for( PlaybackMap::iterator it = m_mPlaybacks.begin( ); it != m_mPlaybacks.end( ); ++it )
	{
		it->second->pSample->RemovePlaybackReference( );
		delete it->second;
	}
	m_mPlaybacks.clear( );

	m_csSlotbuf.leave( );
}

bool ITASoundSamplerImpl::IsPlaybackPaused( int iPlaybackID )
{
	m_csSlotbuf.enter( );
	PlaybackMap::iterator it = m_mPlaybacks.find( iPlaybackID );
	if( it == m_mPlaybacks.end( ) )
	{
		// Playback-ID ist nicht (mehr) g�ltig
		m_csSlotbuf.leave( );
		return false;
	}

	// bool bResult = it->second->bPauseStateNext;
	bool bResult = ( ( it->second->iPauseState == 2 ) || ( it->second->iPauseState == 3 ) );
	m_csSlotbuf.leave( );

	return bResult;
}

void ITASoundSamplerImpl::PausePlayback( int iPlaybackID )
{
	SetPlaybackPaused( iPlaybackID, true );
}

void ITASoundSamplerImpl::ResumePlayback( int iPlaybackID )
{
	SetPlaybackPaused( iPlaybackID, false );
}

void ITASoundSamplerImpl::SetPlaybackPaused( int iPlaybackID, bool bPaused )
{
	m_csSlotbuf.enter( );
	PlaybackMap::iterator it = m_mPlaybacks.find( iPlaybackID );
	if( it == m_mPlaybacks.end( ) )
	{
		// Playback-ID ist nicht (mehr) g�ltig
		m_csSlotbuf.leave( );
		// TODO: Error-Handling? Ausnahme?
		return;
	}

	/*
	if (it->second->bPauseStateNext == bPaused) {
	    // Keine Zustands�nderung
	    m_csSlotbuf.leave();
	    return;
	}

	// Immer den n�chsten Zustand zur�ckgeben, auf wenn dieser noch nicht �bernommen wurde
	it->second->bPauseStateNext = bPaused;
	*/

	ITASoundSamplerPlayback* pPlayback = it->second;
	int iRemainSamples                 = pPlayback->pSample->GetLength( ) - pPlayback->iSamplePosition;

	// Blend-Modus (0 = Normale Wiedergabe, 1 = Ausblenden, 2 = Pausiert, 3 = Einblenden)
	switch( pPlayback->iPauseState )
	{
		case 0:
			if( bPaused )
			{
				// Fall: Normale Wiedergabe. Jetzt pausieren.
				pPlayback->iPauseState      = 1;
				pPlayback->iPauseFadeOffset = 0;
				// Blendl�nge maximal so lang wie Samples verbleiben!
				pPlayback->iPauseFadeLength = ( std::min )( DEFAULT_PAUSE_FADE_LENGTH, iRemainSamples );
			}
			break;

		case 1:
			if( !bPaused )
			{
				// Fall: Ausblenden f�r Pause im Gange. Jetzt Pause wieder aufheben.
				pPlayback->iPauseState      = 3;
				pPlayback->iPauseFadeOffset = pPlayback->iPauseFadeLength - pPlayback->iPauseFadeOffset;
			}
			break;

		case 2:
			if( !bPaused )
			{
				// Fall: Pause aktiv. Jetzt Pause aufheben.
				pPlayback->iPauseState      = 3;
				pPlayback->iPauseFadeOffset = 0;
				// Blendl�nge maximal so lang wie Samples verbleiben!
				pPlayback->iPauseFadeLength = ( std::min )( DEFAULT_PAUSE_FADE_LENGTH, iRemainSamples );
			}
			break;

		case 3:
			if( bPaused )
			{
				// Fall: Pause wird gerade aufgehoben. Jetzt doch wieder pausieren.

				// Hinweis: Fade-L�nge usw. bleiben wie sie sind. Nur die Fade-Richtung wird umgekehrt.
				pPlayback->iPauseState      = 1;
				pPlayback->iPauseFadeOffset = pPlayback->iPauseFadeLength - pPlayback->iPauseFadeOffset;
			}
			break;
	};

	/*
	    if (pPlayback->iPauseFadeOffset == pPlayback->iPauseFadeLength) {
	        // Kein laufendes Fading im Gange
	        pPlayback->iPauseFadeMode = (bPaused ? 0 : 1);
	        pPlayback->iPauseFadeOffset = 0;
	        pPlayback->iPauseFadeLength = DEFAULT_PAUSE_FADE_LENGTH;
	    } else {
	        // Laufendes Fading im Gange + �nderung von Ein- zu Ausblenden bzw. umgekehrt

	        // Blendrichtung umkehren
	        pPlayback->iPauseFadeMode = 1-pPlayback->iPauseFadeMode;

	        // Hinweis: Blendoffset und L�nge bleiben wie sie sind
	    }
	*/

	m_csSlotbuf.leave( );
}

int ITASoundSamplerImpl::AddEvent( double dTimecode, ITASoundSamplerEventHandler* pHandler, int iTag, void* pParam )
{
	// TODO: Implement
	return -1;
}

void ITASoundSamplerImpl::RemoveEvent( int iEventID )
{
	// TODO: Implement
}

void ITASoundSamplerImpl::Print( )
{
	// Spuren
	if( m_mTracks.empty( ) )
		printf( "Keine Spuren angelegt\n\n" );
	else
	{
		printf( "%d Spuren angelegt:\n\n", m_mTracks.size( ) );
		for( TrackMap::iterator it = m_mTracks.begin( ); it != m_mTracks.end( ); ++it )
		{
			Track& track = it->second;
			printf( "\t[%d] ", it->first );
			if( !track.sName.empty( ) )
				printf( "\"%s\" ", track.sName.c_str( ) );

			std::string sGains;
			for( int i = 0; i < track.iChannels; i++ )
			{
				if( i > 0 )
					sGains += ", ";
				sGains += ratio_to_db20_str( track.vdChannelGains[i] );
			}

			printf( "(%d Ch, Gain %s, Routing <%s>, ChGains <%s>)\n", track.iChannels, ratio_to_db20_str( track.dGain ).c_str( ),
			        IntVecToString( track.viChannelRoutings ).c_str( ), sGains.c_str( ) );
		}
		printf( "\n" );
	}

	/*	// Samples
	    if (m_mSamples.empty())
	        printf("Keine Samples geladen\n\n");
	    else {
	        printf("%d Samples geladen:\n\n", m_mSamples.size());
	        for (SampleMap::iterator it=m_mSamples.begin(); it!=m_mSamples.end(); ++it)
	            printf("\t[%d] \"%s\" (%d Kanaele, %d Samples = %0.3fs)\n",
	                   it->first, it->second->GetFilename().c_str(), it->second->GetNumberOfChannels(),
	                   it->second->GetLength(), (double) it->second->GetLength() / ITADatasourceRealization::m_dSamplerate);
	        printf("\n");
	    }
	*/
}

unsigned int ITASoundSamplerImpl::GetBlocklength( ) const
{
	return ITADatasourceRealization::GetBlocklength( );
}

unsigned int ITASoundSamplerImpl::GetNumberOfChannels( ) const
{
	return ITADatasourceRealization::GetNumberOfChannels( );
}

double ITASoundSamplerImpl::GetSampleRate( ) const
{
	return ITADatasourceRealization::GetSampleRate( );
}

const float* ITASoundSamplerImpl::GetBlockPointer( unsigned int uiChannel, const ITAStreamInfo* pStreamInfo )
{
	if( m_bProcess )
	{
		RenderOutputBlock( );
		m_bProcess = false;
	}

	return ITADatasourceRealization::GetBlockPointer( uiChannel, pStreamInfo );
}

void ITASoundSamplerImpl::IncrementBlockPointer( )
{
	m_bProcess = true;
	ITADatasourceRealization::IncrementBlockPointer( );
}

void ITASoundSamplerImpl::RenderOutputBlock( )
{
	// Alle Resourcen sperren
	m_csTracks.enter( );
	m_csSlotbuf.enter( );

	// Ausgabepuffer aller Kan�le Null setzen
	for( unsigned int i = 0; i < ITADatasourceRealization::m_uiChannels; i++ )
		memset( GetWritePointer( i ), 0, ITADatasourceRealization::m_uiBlocklength * sizeof( float ) );

	// Aktuellen Wiedergabeslot (sowie n�chsten) holen
	ITASoundSamplerSlot* m_pCurrentSlot = m_sbSlotbuf.GetSlot( m_iCurrentBlockIndex );
	ITASoundSamplerSlot* m_pNextSlot    = m_sbSlotbuf.GetSlot( m_iCurrentBlockIndex + 1 );

	// Alle Playbacks im Slot bearbeiten
	std::vector<ITASoundSamplerPlayback*>& vpPlaybacks = m_pCurrentSlot->m_vPlaybacks;
	while( !vpPlaybacks.empty( ) )
	{
		ITASoundSamplerPlayback* playback = vpPlaybacks.front( );

		// Pausierungstatus �bernehmen
		// playback->bPauseStateCurrent = playback->bPauseStateNext;

		// bool bStartPause = !playback->bPauseStateCurrent && playback->bPauseStateNext;
		// bool bStopPause = playback->bPauseStateCurrent && !playback->bPauseStateNext;
		// bool bTogglePauseState = bStartPause || bStopPause;

		// Neuen Pausierungstatus �bernehmen
		// playback->bPauseStateCurrent = playback->bPauseStateNext;

		// Playback nicht pausiert bzw. �nderung des Pausestatus -> (Teilweise) Wiedergabe.

		//
		// int iPauseFadeLength = 0;
		// if (bTogglePauseState) {
		//	/*
		//	 *  Sanftes Ein-/Ausblenden des Playbacks
		//	 *  Maximale Blendl�nge beachten (verbleibende Samples insgesamt bzw. in diesem Block )
		//	 */

		//	iPauseFadeLength = (std::min)(DEFAULT_PAUSE_FADE_LENGTH, iRemainSamples);
		//	iPauseFadeLength = (std::min)(iPauseFadeLength, (signed) ITADatasourceRealization::m_uiBlocklength - playback->iPlaybackOffset);

		//	printf("%d\n", iPauseFadeLength);

		//	// Nur beim einsetzen der Pause die Wiedergabe verk�rzen
		//	if (bStartPause) iNumSamples = iPauseFadeLength;
		//}

		if( playback->iPauseState != 2 )
		{
			// Fall: Normales Playback, Pausierung setzt ein bzw. h�rt auf

			Track& track       = m_mTracks[playback->iTrackID];
			double dOuterGain  = m_dMasterGain * track.dGain * playback->dGain;
			int iRemainSamples = playback->pSample->GetLength( ) - playback->iSamplePosition;
			int iFadeRemain    = playback->iPauseFadeLength - playback->iPauseFadeOffset;
			int iNumSamples    = ( std::min )( iRemainSamples, (signed)ITADatasourceRealization::m_uiBlocklength - playback->iPlaybackOffset );

			// Im Falle des Ausblendens die Anzahl Samples zus�tzlich auf die Blendl�nge verk�rzen
			if( playback->iPauseState == 1 )
			{
				iNumSamples = ( std::min )( iNumSamples, iFadeRemain );
			}

			// �ber alle Spurkan�le des Playbacks iterieren
			for( int i = 0; i < track.iChannels; i++ )
			{
				int iOutputChannel       = track.viChannelRoutings[i];
				float* pfOutputData      = GetWritePointer( (unsigned)iOutputChannel );
				const float* pfInputData = playback->pSample->GetChannelData( i );

				// Daten einmischen
				float fGain = (float)( dOuterGain * track.vdChannelGains[i] );

				for( int j = 0; j < iNumSamples; j++ )
					pfOutputData[playback->iPlaybackOffset + j] += pfInputData[playback->iSamplePosition + j] * fGain;

				if( playback->iPauseState == 1 )
				{
					// Fall: Ausblenden
					Fade( pfOutputData + playback->iPlaybackOffset, playback->iPauseFadeLength, ITABase::FadingSign::FADE_OUT, ITABase::FadingFunction::LINEAR,
					      playback->iPauseFadeOffset, iNumSamples );
					playback->iPauseFadeOffset += iNumSamples;
				}

				if( playback->iPauseState == 3 )
				{
					// Fall: Einblenden
					Fade( pfOutputData + playback->iPlaybackOffset, playback->iPauseFadeLength, ITABase::FadingSign::FADE_IN, ITABase::FadingFunction::LINEAR,
					      playback->iPauseFadeOffset, iNumSamples );
					playback->iPauseFadeOffset += iNumSamples;
				}

				// if (bStartPause) fade(pfOutputData + playback->iPlaybackOffset, iPauseFadeLength, ITA_FADE_OUT | ITA_FADE_COSINE_SQUARE);
				// if (bStopPause) fade(pfOutputData + playback->iPlaybackOffset + iNumSamples - iPauseFadeLength, iPauseFadeLength, ITA_FADE_IN |
				// ITA_FADE_COSINE_SQUARE);
			}

			// Flags des Playbacks aktualisieren
			playback->iPlaybackPosition += iNumSamples;
			playback->iPlaybackBlock  = playback->iPlaybackPosition / (signed)ITADatasourceRealization::m_uiBlocklength;
			playback->iPlaybackOffset = playback->iPlaybackPosition % (signed)ITADatasourceRealization::m_uiBlocklength;
			playback->iSamplePosition += iNumSamples;

			// Fading beenden
			if( playback->iPauseFadeOffset >= playback->iPauseFadeLength )
			{
				if( playback->iPauseState == 1 )
					playback->iPauseState = 2;
				if( playback->iPauseState == 3 )
					playback->iPauseState = 0;

				playback->iPauseFadeOffset = playback->iPauseFadeLength = 0;
			}
		}
		else
		{
			// Fall: Pause im Gange. Kein Fading mehr.

			// Playback einfach einen Block weiterschieben
			playback->iPlaybackBlock++;
		}


		//
		//} else {
		//		// Wiedergabe einfach um einen Slot weiterschieben
		//		playback->iPlaybackPosition += (signed) ITADatasourceRealization::m_uiBlocklength;
		//		playback->iPlaybackBlock++;
		//	}

		vpPlaybacks.erase( vpPlaybacks.begin( ) );
		if( playback->iSamplePosition < playback->pSample->GetLength( ) )
		{
			// Noch nicht fertig abgespieltes Playback in den n�chstfolgenden Slot platzieren
			m_pNextSlot->m_vPlaybacks.push_back( playback );
		}
		else
		{
			// Playback kann gel�scht werden -> Referenz auf dem Sample entfernen
			playback->pSample->RemovePlaybackReference( );
		}
	}

	ITADatasourceRealization::IncrementWritePointer( );
	m_sbSlotbuf.IncrementPlaybackBlock( );
	m_iCurrentBlockIndex++;

	// Alle Resourcen freigeben
	m_csSlotbuf.leave( );
	m_csTracks.leave( );
}

/*
 *      +------------------+
 *  --->| ITA Sampler      |--->
 *  --->| o o o o      /// |--->
 *      +------------------+
 *
 *  ITASampler ist eine C++ Bibliothek die einen Sound sampler zur wavetable-basierten
 *  Klangsynthese bereitstellt. Der Begriff "sampler" bezieht sich hier auf den Terminus
 *  aus der Musikproduktion und nicht auf Abtastwerte.
 *
 *  Datei:		ITASoundSampleImpl.h
 *  Zweck:		Implementierungsklasse f�r sound samples
 *  Autoren:	Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *  CVS-Datum:	$Id: ITASoundSampleImpl.h,v 1.1 2008-12-10 14:15:03 fwefers Exp $
 *
 *  (c) Copyright Institut f�r Technische Akustik (ITA) RWTH Aachen, 2008
 */

#ifndef INCLUDE_WATCHER_ITA_SOUND_SAMPLE_IMPL
#define INCLUDE_WATCHER_ITA_SOUND_SAMPLE_IMPL

#include <ITACriticalSection.h>
#include <ITASoundSample.h>
#include <string>
#include <vector>

/**
 * Diese Realisierung des SoundSamples wird nur intern von
 * der SoundSamplePool benutzt.
 */

class ITASoundSampleImpl : public ITASoundSample
{
public:
	/* Sample aus Datei laden
	 *
	 * iMaxNumberOfChannels = Maximal m�gliche Kanalanzahl bestimmt durch die Ausgangskan�le des Samplers
	 *                        Wird zur Fehler�berpr�fung benutzt
	 * dRequiredSamplerate = Abtastrate des Samplers zwingend erforderlich
	 */
	ITASoundSampleImpl( const std::string& sFilename, int iMaxNumberOfChannels, double dRequiredSamplerate, std::string sName = "" );

	/* Sample aus Puffer erzeugen
	 *
	 * Hinweis: Die Sample-Daten werden kopiert.
	 */
	ITASoundSampleImpl( const float** ppfChannelData, int iNumberOfChannels, int iLength, std::string sName = "" );

	~ITASoundSampleImpl( );

	bool OriginFile( ) const;
	std::string GetName( ) const;
	void SetName( const std::string& sName );
	std::string GetFilename( ) const;
	int GetNumberOfChannels( ) const;
	int GetLength( ) const;

	// Zeiger auf die Daten eines Kanals zur�ckgeben
	const float* GetChannelData( int iChannel ) const;

	// Anzahl der vermerkten Playbacks zur�ckgeben
	bool HasPlaybackReferences( );

	// Weiteres Playback vermerken
	void AddPlaybackReference( );

	// Playback entfernen
	void RemovePlaybackReference( );

private:
	std::string m_sName;
	std::string m_sFilename;
	bool m_bOriginFile;
	int m_iLength;
	std::vector<float*> m_vpData;

	int m_iPlaybackRefCount;                 // Referenzz�hler f�r die Anzahl der Playbacks
	ITACriticalSection m_csPlaybackRefCount; // Globales Lock f�r den
};

#endif // INCLUDE_WATCHER_ITA_SOUND_SAMPLE_IMPL

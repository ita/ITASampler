/*
 *      +------------------+
 *  --->| ITA Sampler      |--->
 *  --->| o o o o      /// |--->
 *      +------------------+
 *
 *  ITASampler ist eine C++ Bibliothek die einen Sound sampler zur wavetable-basierten
 *  Klangsynthese bereitstellt. Der Begriff "sampler" bezieht sich hier auf den Terminus
 *  aus der Musikproduktion und nicht auf Abtastwerte.
 *
 *  Datei:		ITASoundSamplerImpl.cpp
 *  Zweck:		Implementierungsklasse des sound samplers
 *  Autoren:	Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *  CVS-Datum:	$Id: ITASoundSampleImpl.cpp,v 1.1 2008-12-10 14:15:03 fwefers Exp $
 *
 *  (c) Copyright Institut f�r Technische Akustik (ITA) RWTH Aachen, 2008
 */
#include "ITASoundSampleImpl.h"

#include <ITAAudiofileReader.h>
#include <ITAException.h>
#include <ITAFunctors.h>
#include <algorithm>
#include <cstring>

ITASoundSampleImpl::ITASoundSampleImpl( const std::string& sFilename, int iMaxNumberOfChannels, double dRequiredSamplerate, std::string sName )
    : m_sName( sName )
    , m_sFilename( sFilename )
    , m_iLength( 0 )
    , m_iPlaybackRefCount( 0 )
    , m_bOriginFile( true )
{
	ITAAudiofileReader* pReader = ITAAudiofileReader::create( sFilename );

	if( pReader->getDomain( ) != ITADomain::ITA_TIME_DOMAIN )
	{
		delete pReader;
		ITA_EXCEPT1( INVALID_PARAMETER, "Audiofile is not in time domain" );
	}

	if( pReader->getNumberOfChannels( ) > iMaxNumberOfChannels )
	{
		delete pReader;
		ITA_EXCEPT1( INVALID_PARAMETER, "Audiofile has too much channels" );
	}

	if( pReader->getSamplerate( ) != dRequiredSamplerate )
	{
		delete pReader;
		ITA_EXCEPT1( INVALID_PARAMETER, "Audiofile has invalid sampling rate" );
	}

	m_iLength = (signed)pReader->getLength( );
	m_vpData  = pReader->read( pReader->getLength( ) );

	delete pReader;
}

ITASoundSampleImpl::ITASoundSampleImpl( const float** ppfChannelData, int iNumberOfChannels, int iLength, std::string sName )
    : m_sName( sName )
    , m_iLength( iLength )
    , m_iPlaybackRefCount( 0 )
    , m_bOriginFile( false )
{
	// Kopie der Daten erzeugen
	m_vpData.resize( iNumberOfChannels, NULL );
	for( int i = 0; i < iNumberOfChannels; i++ )
	{
		m_vpData[i] = new float[iLength];
		memcpy( m_vpData[i], ppfChannelData[i], iLength * sizeof( float ) );
	}
}

ITASoundSampleImpl::~ITASoundSampleImpl( )
{
	std::for_each( m_vpData.begin( ), m_vpData.end( ), arrayDeleteFunctor<float> );
}

bool ITASoundSampleImpl::OriginFile( ) const
{
	return m_bOriginFile;
}

std::string ITASoundSampleImpl::GetName( ) const
{
	return m_sName;
}

void ITASoundSampleImpl::SetName( const std::string& sName )
{
	m_sName = sName;
}

std::string ITASoundSampleImpl::GetFilename( ) const
{
	return m_sFilename;
}

int ITASoundSampleImpl::GetNumberOfChannels( ) const
{
	return (int)m_vpData.size( );
}

int ITASoundSampleImpl::GetLength( ) const
{
	return m_iLength;
}

const float* ITASoundSampleImpl::GetChannelData( int iChannel ) const
{
	return m_vpData[iChannel];
}

bool ITASoundSampleImpl::HasPlaybackReferences( )
{
	m_csPlaybackRefCount.enter( );
	bool bResult = ( m_iPlaybackRefCount > 0 );
	m_csPlaybackRefCount.leave( );
	return bResult;
}

void ITASoundSampleImpl::AddPlaybackReference( )
{
	m_csPlaybackRefCount.enter( );
	m_iPlaybackRefCount++;
	m_csPlaybackRefCount.leave( );
}

void ITASoundSampleImpl::RemovePlaybackReference( )
{
	m_csPlaybackRefCount.enter( );
	if( m_iPlaybackRefCount > 0 )
		m_iPlaybackRefCount--;
	m_csPlaybackRefCount.leave( );
}

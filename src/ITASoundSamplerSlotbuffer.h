
#ifndef INCLUDE_WATCHER_ITA_SOUND_SAMPLER_SLOT_BUFFER
#define INCLUDE_WATCHER_ITA_SOUND_SAMPLER_SLOT_BUFFER

#include "ITASoundSamplerSlot.h"

#include <string>
#include <vector>

class ITASoundSamplerSlotbuffer
{
public:
	ITASoundSamplerSlotbuffer( );
	~ITASoundSamplerSlotbuffer( );

	// Gr��e des Puffers �ndern
	void Resize( int iNumSlots );

	// Slot zur�ckgeben (beinhaltet implizites Resizing)
	ITASoundSamplerSlot* GetSlot( int iBlockIndex );

	// Alle Playbacks eines Samples l�schen und freigeben
	void DeletePlaybacksOfSample( int iSampleID );

	// Alle Playbacks einer Spur l�schen und freigeben
	void DeletePlaybacksOfTrack( int iTrackID );

	// Alle Playbacks l�schen und freigeben
	void DeleteAllPlaybacks( );

	// Leseposition weitersetzen
	void IncrementPlaybackBlock( );

private:
	int m_iSize;
	int m_iPlaybackCursor;
	int m_iPlaybackBlock;
	std::vector<ITASoundSamplerSlot> m_vSlots;
};

#endif // INCLUDE_WATCHER_ITA_SOUND_SAMPLER_SLOT_BUFFER

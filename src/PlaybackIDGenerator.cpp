/*
 *      +------------------+
 *  --->| ITA Sampler      |--->
 *  --->| o o o o      /// |--->
 *      +------------------+
 *
 *  ITASampler ist eine C++ Bibliothek die einen Sound sampler zur wavetable-basierten
 *  Klangsynthese bereitstellt. Der Begriff "sampler" bezieht sich hier auf den Terminus
 *  aus der Musikproduktion und nicht auf Abtastwerte.
 *
 *  Datei:		PlaybackIDGenerator.cpp
 *  Zweck:		Globaler Generator f�r eindeutige Playback-IDs f�r alle Sampler
 *  Autoren:	Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *  CVS-Datum:	$Id: PlaybackIDGenerator.cpp,v 1.1 2008-12-11 20:34:34 fwefers Exp $
 *
 *  (c) Copyright Institut f�r Technische Akustik (ITA) RWTH Aachen, 2008
 */

#include <ITACriticalSection.h>

static int iGlobalPlaybackIDCounter = 1;
static ITACriticalSection csGlobalPlaybackIDCounter;

int generatePlaybackID( )
{
	csGlobalPlaybackIDCounter.enter( );
	int iResult = iGlobalPlaybackIDCounter++;
	csGlobalPlaybackIDCounter.leave( );
	return iResult;
}

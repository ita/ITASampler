#ifndef INCLUDE_WATCHER_ITA_SOUND_SAMPLER_IMPL
#define INCLUDE_WATCHER_ITA_SOUND_SAMPLER_IMPL

#include "ITASoundSamplerSlotbuffer.h"

#include <ITACriticalSection.h>
#include <ITADataSourceRealization.h>
#include <ITASoundSampler.h>
#include <map>
#include <string>
#include <vector>

class ITADatasourceRealization;
class ITASoundSampleImpl;
class ITASoundSamplePoolImpl;

class ITASoundSamplerImpl
    : public ITASoundSampler
    , private ITADatasourceRealization
{
public:
	ITASoundSamplerImpl( int iOutputChannels, double dSamplerate, int iBlocklength, ITASoundSamplePool* pSamplepool = NULL );
	~ITASoundSamplerImpl( );

	void SetSampleClock( ITASampleClock* pClock );
	ITASoundSamplePool* GetSamplePool( ) const;

	void ResetSampleCount( );
	int GetSampleCount( );

	int AddTrack( int iTrackChannels );
	void RouteTrackChannel( int iTrackID, int iTrackChannel, int iOutputChannel );
	int AddMonoTrack( int iOutputChannel = 0 );
	int AddStereoTrack( int iLeftOutputChannel = 0, int iRightOutputChannel = 1 );
	void RemoveTrack( int iTrackID );
	std::string GetTrackName( int iTrackID );
	void SetTrackName( int iTrackID, const std::string& sName );
	int GetTrackNumberOfChannels( int iTrackID );

	double GetMasterGain( );
	void SetMasterGain( double dGain );
	double GetTrackGain( int iTrackID );
	void SetTrackGain( int iTrackID, double dGain );
	double GetTrackChannelGain( int iTrackID, int iTrackChannel );
	void SetTrackChannelGain( int iTrackID, int iTrackChannel, double dGain );

	int CreateSample( const float* pfData, int iLength, double dSamplerate, std::string sName = "" );
	int CreateSample( const float* pfLeftChannelData, const float* pfRightChannelData, int iLength, double dSamplerate, std::string sName = "" );
	int CreateSample( const float** ppfChannelData, int iNumChannels, int iLength, double dSamplerate, std::string sName = "" );
	int CreateSample( std::vector<float*> vpfChannelData, int iLength, double dSamplerate, std::string sName = "" );
	int LoadSample( const std::string& sFilename, std::string sName = "" );
	void FreeSample( int iSampleID );

	int AddPlaybackBySamplecount( int iSampleID, int iTrackID, int iSamplecount, bool bSetPaused = false, double dGain = 1.0 );
	int AddPlaybackByTimecode( int iSampleID, int iTrackID, double dTimecode, bool bSetPaused = false, double dGain = 1.0 );
	void RemovePlayback( int iPlaybackID );
	void RemovePlaybacksOfTrack( int iTrackID );
	void RemoveAllPlaybacks( );
	bool IsPlaybackPaused( int iPlaybackID );
	void SetPlaybackPaused( int iPlaybackID, bool bPaused );
	void PausePlayback( int iPlaybackID );
	void ResumePlayback( int iPlaybackID );

	int AddEvent( double dTimecode, ITASoundSamplerEventHandler* pHandler, int iTag, void* pParam );
	void RemoveEvent( int iEventID );

	void Print( );

	//! Realisierung der Schnittstelle von ITADatasource
	unsigned int GetBlocklength( ) const;
	unsigned int GetNumberOfChannels( ) const;
	double GetSampleRate( ) const;

	const float* GetBlockPointer( unsigned int uiChannel, const ITAStreamInfo* pStreamInfo );
	void IncrementBlockPointer( );

private:
	// Interne Datenklasse die Spuren beschreibt
	class Track
	{
	public:
		std::string sName;
		int iChannels;                      // Anzahl Kan�le
		double dGain;                       // Lautst�rke der Spur insgesamt
		std::vector<int> viChannelRoutings; // Ausgabe Kan�le der einzelnen Spurkan�le
		std::vector<double> vdChannelGains; // Lautst�rken der Spurkan�le

		inline Track( ) { };

		inline Track( int iChannels )
		{
			/*
			 *  Standardlautst�rke f�r die Spur und alle ihre Kan�le = 0 dB.
			 *  Standardm��ig werden die Kan�le der Spur linear auf die Ausgangskan�le abgebildet
			 */
			this->iChannels = iChannels;
			dGain           = 1.0;
			viChannelRoutings.resize( iChannels );
			for( int i = 0; i < iChannels; i++ )
				viChannelRoutings[i] = i;
			vdChannelGains.resize( iChannels, 1.0 );
		};
	};

	typedef std::map<int, Track> TrackMap;
	typedef std::map<int, ITASoundSamplerPlayback*> PlaybackMap;

	TrackMap m_mTracks;
	PlaybackMap m_mPlaybacks;

	ITASoundSamplerSlotbuffer m_sbSlotbuf;

	ITACriticalSection m_csTracks;
	ITACriticalSection m_csSlotbuf;

	int m_iTrackIDCount;      // Globaler Z�hler f�r Track-IDs
	int m_iCurrentBlockIndex; // Nummer des aktuell abgespielten Blocks;
	bool m_bProcess;          // Erster Eintritt in GBP nach letzem IBP -> Daten erzeugen
	double m_dMasterGain;

	ITASampleClock* m_pSampleClock;
	ITASampleClock* m_pDefaultSampleClock;
	ITASoundSamplePoolImpl* m_pDefaultSamplePool;
	ITASoundSamplePoolImpl* m_pSamplePool; // Zugriffszeiger auf den Sample pool

	// Ausgabeblock rendern
	void RenderOutputBlock( );
};

#endif // INCLUDE_WATCHER_ITA_SOUND_SAMPLER_IMPL

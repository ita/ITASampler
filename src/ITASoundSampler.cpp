#include "ITASoundSamplerImpl.h"

#include <ITASoundSampler.h>

ITASoundSampler* ITASoundSampler::Create( int iOutputChannels, double dSamplerate, int iBlocklength, ITASoundSamplePool* pSamplepool )
{
	return new ITASoundSamplerImpl( iOutputChannels, dSamplerate, iBlocklength, pSamplepool );
}

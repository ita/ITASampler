
#ifndef INCLUDE_WATCHER_ITA_SOUND_SAMPLER_PLAYBACK
#define INCLUDE_WATCHER_ITA_SOUND_SAMPLER_PLAYBACK

#include <vector>
class ITASoundSampleImpl;

class ITASoundSamplerPlayback
{
public:
	int iID;                     // Playback ID
	int iTrackID;                // Zielspur
	int iSampleID;               // Sample ID
	ITASoundSampleImpl* pSample; // Sample Instanz
	int iSamplePosition;         // Abzuspielendes Sample (Abtastwert) im Sample (Klang)
	int iPlaybackPosition;       // Wiedergabeposition (Sample aka. Abtastwert) im Datenstrom
	int iPlaybackBlock;          // Slot im übergeordneten Slotpuffer
	int iPlaybackOffset;         // Wiedergabeposition im Block
	double dGain;                // Lautstärke

	// bool bPauseStateCurrent;		// Momentan umgesetzter Pause-Zustand (Pausiert ja/nein)
	// bool bPauseStateNext;			// Neuer Pause-Zustand
	int iPauseState;      // Blend-Modus (0 = Normale Wiedergabe, 1 = Ausblenden, 2 = Pausiert, 3 = Einblenden)
	int iPauseFadeOffset; // Position im Blendprozess
	int iPauseFadeLength; // Länge der Blendung
};

#endif // INCLUDE_WATCHER_ITA_SOUND_SAMPLER_PLAYBACK

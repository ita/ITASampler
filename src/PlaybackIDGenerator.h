#ifndef INCLUDE_WATCHER_ITA_PLAYBACK_ID_GENERATOR
#define INCLUDE_WATCHER_ITA_PLAYBACK_ID_GENERATOR

// Erzeugt eine unter allen Samplern eindeutige Playback-ID
int generatePlaybackID( );

#endif // INCLUDE_WATCHER_ITA_PLAYBACK_ID_GENERATOR

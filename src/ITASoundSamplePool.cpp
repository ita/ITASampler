#include "ITASoundSamplePoolImpl.h"

#include <ITASoundSamplePool.h>

ITASoundSamplePool* ITASoundSamplePool::Create( int iMaxNumberOfChannels, double dSamplerate )
{
	return new ITASoundSamplePoolImpl( iMaxNumberOfChannels, dSamplerate );
}

#include "Sample.h"

#include <ITAAudiofileReader.h>
#include <ITAException.h>
#include <ITAFunctors.h>
#include <algorithm>

Sample::Sample( const std::string& sFilename, int iMaxNumberOfChannels, double dRequiredSamplerate ) : m_sFilename( sFilename ), m_iLength( 0 )
{
	ITAAudiofileReader* pReader = ITAAudiofileReader::create( sFilename );

	if( pReader->getDomain( ) != ITA_TIME_DOMAIN )
	{
		delete pReader;
		ITA_EXCEPT1( INVALID_PARAMETER, "Audiofile is not in time domain" );
	}

	if( pReader->getNumberOfChannels( ) > (unsigned)iMaxNumberOfChannels )
	{
		delete pReader;
		ITA_EXCEPT1( INVALID_PARAMETER, "Audiofile has too much channels" );
	}

	if( pReader->getSamplerate( ) != dRequiredSamplerate )
	{
		delete pReader;
		ITA_EXCEPT1( INVALID_PARAMETER, "Audiofile has invalid sampling rate" );
	}

	m_iLength = (signed)pReader->getLength( );
	m_vpData  = pReader->read( pReader->getLength( ) );

	delete pReader;
}

Sample::~Sample( )
{
	std::for_each( m_vpData.begin( ), m_vpData.end( ), arrayDeleteFunctor<float> );
}

std::string Sample::GetFilename( )
{
	return m_sFilename;
}

int Sample::GetNumberOfChannels( )
{
	return (int)m_vpData.size( );
}

int Sample::GetLength( )
{
	return m_iLength;
}

float* Sample::GetChannelData( int iChannel )
{
	return m_vpData[iChannel];
}

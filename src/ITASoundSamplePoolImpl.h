#ifndef INCLUDE_WATCHER_ITA_SOUND_SAMPLE_POOL_IMPL
#define INCLUDE_WATCHER_ITA_SOUND_SAMPLE_POOL_IMPL

#include "ITASoundSampleImpl.h"

#include <ITACriticalSection.h>
#include <ITASoundSamplePool.h>
#include <map>
#include <string>

class ITASoundSamplePoolImpl : public ITASoundSamplePool
{
public:
	ITASoundSamplePoolImpl( int iMaxNumberOfChannels, double dSamplerate );
	~ITASoundSamplePoolImpl( );

	int GetMaximumNumberOfChannels( ) const;
	double GetSampleRate( ) const;
	int CreateSample( const float* pfData, int iLength, double dSamplerate, std::string sName = "" );
	int CreateSample( const float* pfLeftChannelData, const float* pfRightChannelData, int iLength, double dSamplerate, std::string sName = "" );
	int CreateSample( const float** ppfChannelData, int iNumChannels, int iLength, double dSamplerate, std::string sName = "" );
	int CreateSample( std::vector<float*> vpfChannelData, int iLength, double dSamplerate, std::string sName = "" );
	int LoadSample( const std::string& sFilename, std::string sName = "" );
	void FreeSample( int iSampleID );
	const ITASoundSample* GetSample( int iSampleID, bool bAddPlaybackReference );
	void GetSampleIDs( std::vector<int>& viSampleIDs ) const;

private:
	typedef std::map<int, ITASoundSampleImpl*> SampleMap;

	SampleMap m_mSamples;           // Kontainer f�r Sound Samples (ID -> Instanz)
	ITACriticalSection m_csSamples; // Lokales Lock f�r den Sample-Container

	double m_dSampleRate;       // Abtastrate [Hz]
	int m_iMaxNumberOfChannels; // Maximale Kanalanzahl

	static int m_iSampleIDCount;                 // Globaler Z�hler f�r Sample-IDs
	static ITACriticalSection m_csSampleIDCount; // Globale Synchronisation f�r den Z�hler
};

#endif // INCLUDE_WATCHER_ITA_SOUND_SAMPLE_POOL_IMPL

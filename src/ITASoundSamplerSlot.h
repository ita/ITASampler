
#ifndef INCLUDE_WATCHER_ITA_SOUND_SAMPLER_SLOT
#define INCLUDE_WATCHER_ITA_SOUND_SAMPLER_SLOT

#include "ITASoundSampleImpl.h"
#include "ITASoundSamplerPlayback.h"

#include <vector>

class ITASoundSamplerSlot
{
public:
	std::vector<ITASoundSamplerPlayback*> m_vPlaybacks;
	// std::vector<ITASamplerEvent*> m_vpEvents;

	ITASoundSamplerSlot( )
	{
		// Platz f�r bis zu 16 Playbacks vorreservieren
		m_vPlaybacks.reserve( 16 );

		Clear( );
	}

	// Alle Playbacks und Events l�schen, Werte zur�cksetzen;
	void Clear( )
	{
		// m_iBlockIndex = 0;
		// m_iSampleIndex = 0;
		m_vPlaybacks.clear( );
		// TODO: Playbacks freigeben
	}

	// Entfernt alle Playbacks eines Sampels aus dem Slot und gibt diese Playbacks auch frei (delete)
	void DeletePlaybacksOfSample( int iSampleID )
	{
		for( std::vector<ITASoundSamplerPlayback*>::iterator it = m_vPlaybacks.begin( ); it != m_vPlaybacks.end( ); )
		{
			ITASoundSamplerPlayback* pPlayback = *it;
			if( pPlayback->iSampleID == iSampleID )
			{
				m_vPlaybacks.erase( it );
				delete pPlayback;
			}
			else
				++it;
		}
	}

	// Entfernt alle Playbacks einer Spur aus dem Slot und gibt diese Playbacks auch frei (delete)
	void DeletePlaybacksOfTrack( int iTrackID )
	{
		for( std::vector<ITASoundSamplerPlayback*>::iterator it = m_vPlaybacks.begin( ); it != m_vPlaybacks.end( ); )
		{
			ITASoundSamplerPlayback* pPlayback = *it;
			if( pPlayback->iTrackID == iTrackID )
			{
				m_vPlaybacks.erase( it );
				pPlayback->pSample->RemovePlaybackReference( );
				delete pPlayback;
			}
			else
				++it;
		}
	}
};

#endif // INCLUDE_WATCHER_ITA_SOUND_SAMPLER_SLOT

/*
 *      +------------------+
 *  --->| ITA Sampler      |--->
 *  --->| o o o o      /// |--->
 *      +------------------+
 *
 *  ITASampler ist eine C++ Bibliothek die einen Sound sampler zur wavetable-basierten
 *  Klangsynthese bereitstellt. Der Begriff "sampler" bezieht sich hier auf den Terminus
 *  aus der Musikproduktion und nicht auf Abtastwerte.
 *
 *  Datei:		ITASoundSamplePoolImpl.cpp
 *  Zweck:		Implementierungsklasse f�r Kontainer/Verwalter von sound samples
 *  Autoren:	Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *  CVS-Datum:	$Id: ITASoundSamplePoolImpl.cpp,v 1.1 2008-12-10 14:15:03 fwefers Exp $
 *
 *  (c) Copyright Institut f�r Technische Akustik (ITA) RWTH Aachen, 2008
 */

#include "ITASoundSamplePoolImpl.h"

#include <ITAException.h>

int ITASoundSamplePoolImpl::m_iSampleIDCount = 1;
ITACriticalSection ITASoundSamplePoolImpl::m_csSampleIDCount;

ITASoundSamplePoolImpl::ITASoundSamplePoolImpl( int iMaxNumberOfChannels, double dSamplerate )
{
	m_iMaxNumberOfChannels = iMaxNumberOfChannels;
	m_dSampleRate          = dSamplerate;
}

ITASoundSamplePoolImpl::~ITASoundSamplePoolImpl( )
{
	m_csSamples.enter( );

	// Zun�chst pr�fen, ob alle Samples unbenutzt sind
	for( SampleMap::iterator it = m_mSamples.begin( ); it != m_mSamples.end( ); ++it )
		if( it->second->HasPlaybackReferences( ) )
			ITA_EXCEPT1( INVALID_PARAMETER, "Some samples are still in use." );

	// Alle Samples l�schen
	for( SampleMap::iterator it = m_mSamples.begin( ); it != m_mSamples.end( ); ++it )
		delete it->second;

	m_csSamples.leave( );
}

int ITASoundSamplePoolImpl::GetMaximumNumberOfChannels( ) const
{
	return m_iMaxNumberOfChannels;
}

double ITASoundSamplePoolImpl::GetSampleRate( ) const
{
	return m_dSampleRate;
}

int ITASoundSamplePoolImpl::CreateSample( const float* pfData, int iLength, double dSamplerate, std::string sName )
{
	const float* ppfChannelData[1] = { pfData };
	return CreateSample( ppfChannelData, 1, iLength, dSamplerate, sName );
}

int ITASoundSamplePoolImpl::CreateSample( const float* pfLeftChannelData, const float* pfRightChannelData, int iLength, double dSamplerate, std::string sName )
{
	const float* ppfChannelData[2] = { pfLeftChannelData, pfRightChannelData };
	return CreateSample( ppfChannelData, 2, iLength, dSamplerate, sName );
}

int ITASoundSamplePoolImpl::CreateSample( const float** ppfChannelData, int iNumChannels, int iLength, double dSamplerate, std::string sName )
{
	if( dSamplerate != m_dSampleRate )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid sampling rate" );

	if( iNumChannels > m_iMaxNumberOfChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Too many channels" );

	m_csSamples.enter( );

	// Versuchen das Sample zu laden
	ITASoundSampleImpl* pSample = new ITASoundSampleImpl( ppfChannelData, iNumChannels, iLength, sName );

	m_csSampleIDCount.enter( );
	int iSampleID = m_iSampleIDCount++;
	m_csSampleIDCount.leave( );

	m_mSamples.insert( std::pair<int, ITASoundSampleImpl*>( iSampleID, pSample ) );

	m_csSamples.leave( );

	return iSampleID;
}

int ITASoundSamplePoolImpl::CreateSample( std::vector<float*> vpfChannelData, int iLength, double dSamplerate, std::string sName )
{
	return CreateSample( (const float**)vpfChannelData.front( ), (int)vpfChannelData.size( ), iLength, dSamplerate, sName );
}

int ITASoundSamplePoolImpl::LoadSample( const std::string& sFilename, std::string sName )
{
	m_csSamples.enter( );

	// Zun�chst Pr�fen, ob bereits in Sample f�r diese Datei geladen wurde
	for( SampleMap::iterator it = m_mSamples.begin( ); it != m_mSamples.end( ); ++it )
		if( it->second->GetFilename( ) == sFilename )
		{
			m_csSamples.leave( );
			return it->first;
		}

	// Versuchen das Sample zu laden
	ITASoundSampleImpl* pSample = new ITASoundSampleImpl( sFilename, m_iMaxNumberOfChannels, m_dSampleRate, sName );

	m_csSampleIDCount.enter( );
	int iSampleID = m_iSampleIDCount++;
	m_csSampleIDCount.leave( );

	m_mSamples.insert( std::pair<int, ITASoundSampleImpl*>( iSampleID, pSample ) );

	m_csSamples.leave( );

	return iSampleID;
}

void ITASoundSamplePoolImpl::FreeSample( int iSampleID )
{
	m_csSamples.enter( );

	SampleMap::iterator it = m_mSamples.find( iSampleID );
	if( it != m_mSamples.end( ) )
	{
		ITASoundSampleImpl* pSample = it->second;

		if( pSample->HasPlaybackReferences( ) != 0 )
		{
			// Sample ist noch in Benutzung
			m_csSamples.leave( );
			ITA_EXCEPT1( INVALID_PARAMETER, "Sample is still in use" );
			return;
		}

		m_mSamples.erase( it );
		delete pSample;
	}

	m_csSamples.leave( );
}

const ITASoundSample* ITASoundSamplePoolImpl::GetSample( int iSampleID, bool bAddPlaybackReference )
{
	m_csSamples.enter( );

	SampleMap::iterator it      = m_mSamples.find( iSampleID );
	ITASoundSampleImpl* pResult = ( it != m_mSamples.end( ) ? it->second : NULL );

	if( pResult && bAddPlaybackReference )
		pResult->AddPlaybackReference( );

	m_csSamples.leave( );

	return pResult;
}

void ITASoundSamplePoolImpl::GetSampleIDs( std::vector<int>& viSampleIDs ) const
{
	m_csSamples.enter( );

	// Liste der IDs kopieren
	viSampleIDs.reserve( m_mSamples.size( ) );
	viSampleIDs.clear( );
	for( SampleMap::const_iterator cit = m_mSamples.begin( ); cit != m_mSamples.end( ); ++cit )
		viSampleIDs.push_back( cit->first );

	m_csSamples.leave( );
}
/*
 *      +------------------+
 *  --->| ITA Sampler      |--->
 *  --->| o o o o      /// |--->
 *      +------------------+
 *
 *  ITASampler ist eine C++ Bibliothek die einen Sound sampler zur wavetable-basierten
 *  Klangsynthese bereitstellt. Der Begriff "sampler" bezieht sich hier auf den Terminus
 *  aus der Musikproduktion und nicht auf Abtastwerte.
 *
 *  Datei:		ITASoundSamplerSlotbuffer.cpp
 *  Zweck:		Interne Klasse welche komplette Puffer f�r Playbacks realisiert
 *  Autoren:	Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *  CVS-Datum:	$Id: ITASoundSamplerSlotbuffer.cpp,v 1.2 2008-12-11 20:34:34 fwefers Exp $
 *
 *  (c) Copyright Institut f�r Technische Akustik (ITA) RWTH Aachen, 2008
 */

#include "ITASoundSamplerSlotbuffer.h"

ITASoundSamplerSlotbuffer::ITASoundSamplerSlotbuffer( ) : m_iSize( 0 ), m_iPlaybackCursor( 0 ), m_iPlaybackBlock( 0 )
{
	/*	const int N=256;
	    m_vSlots.resize(N);
	    for (int i=0; i<N; i++) {
	        m_vSlots[i].m_iBlockIndex = 0;
	    }*/
}

ITASoundSamplerSlotbuffer::~ITASoundSamplerSlotbuffer( ) {}

void ITASoundSamplerSlotbuffer::Resize( int iNumSlots )
{
	int iOldSize = m_iSize;
	m_iSize      = iNumSlots;
	m_vSlots.resize( iNumSlots );

	// TODO: Langsame Implementierung. Es muss nicht alles kopiert werden!
	// Slot Inhalte im Bereich 0-Cursor ans Ende kopieren
	for( int i = 0; i < m_iPlaybackCursor; i++ )
	{
		// Von vorne -> Nach hinten kopieren
		int iDest       = ( iOldSize + i ) % m_iSize;
		m_vSlots[iDest] = m_vSlots[i];

		// Verwaisten Slot leeren
		m_vSlots[i].Clear( );
	}
}

ITASoundSamplerSlot* ITASoundSamplerSlotbuffer::GetSlot( int iBlockIndex )
{
	int iDelta        = iBlockIndex - m_iPlaybackBlock;
	int iRequiredSize = iDelta + 1;

	if( iRequiredSize > m_iSize )
		Resize( iRequiredSize );

	return &m_vSlots[( m_iPlaybackCursor + iDelta ) % m_iSize];
}

void ITASoundSamplerSlotbuffer::DeletePlaybacksOfSample( int iSampleID )
{
	for( std::vector<ITASoundSamplerSlot>::iterator it = m_vSlots.begin( ); it != m_vSlots.end( ); ++it )
		it->DeletePlaybacksOfSample( iSampleID );
}

void ITASoundSamplerSlotbuffer::DeletePlaybacksOfTrack( int iTrackID )
{
	for( std::vector<ITASoundSamplerSlot>::iterator it = m_vSlots.begin( ); it != m_vSlots.end( ); ++it )
		it->DeletePlaybacksOfTrack( iTrackID );
}

void ITASoundSamplerSlotbuffer::DeleteAllPlaybacks( )
{
	for( std::vector<ITASoundSamplerSlot>::iterator it = m_vSlots.begin( ); it != m_vSlots.end( ); ++it )
		it->Clear( );
}

void ITASoundSamplerSlotbuffer::IncrementPlaybackBlock( )
{
	m_iPlaybackBlock++;
	m_iPlaybackCursor++;
	m_iPlaybackCursor %= m_iSize;
}

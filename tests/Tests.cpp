#include <ITADataSourceUtils.h>
#include <ITAException.h>
#include <ITANumericUtils.h>
#include <ITASampleClock.h>
#include <ITASoundSampler.h>
#include <iostream>
#include <time.h>

// Zufalls-Ganzzahl im Interval [0, n-1] zur�ckgeben
int irandx( int n )
{
	return rand( ) % n;
}

// Zufalls-Match im Interval [0, n-1] zur�ckgeben
bool randmatch( double prob )
{
	return ( rand( ) % ( (int)ceil( 1 / prob ) ) ) == 0;
}

// Zufalls-Lautst�rke (Intervall [-15 dB, 0 dB], Aufl�sung 1 dB) zur�ckgeben (Faktor)
double randgain( )
{
	return db20_to_ratio( -irandx( 15 + 1 ) );
}

double randdisp( )
{
	return rand( ) % 2 - 1;
}

int main( int argc, char* argv[] )
{
	srand( (unsigned)time( NULL ) );

	ITASoundSampler* sampler = NULL;

	try
	{
		sampler = ITASoundSampler::Create( 2, 44100, 512 );

		// Viervierteltakt!
		double bpm = 220;
		ITASampleClock sclock( sampler->GetSampleRate( ), 4 * 60.0 / bpm );
		sampler->SetSampleClock( &sclock );

		const int TRACK_CRASH = sampler->AddStereoTrack( );
		const int TRACK_HIHAT = sampler->AddStereoTrack( );
		const int TRACK_SNARE = sampler->AddStereoTrack( );
		const int TRACK_KICK  = sampler->AddStereoTrack( );
		const int TRACK_TOMS  = sampler->AddStereoTrack( );

		sampler->SetTrackName( TRACK_CRASH, "Crash" );
		sampler->SetTrackGain( TRACK_CRASH, db20_to_ratio( -12 ) );

		sampler->SetTrackName( TRACK_HIHAT, "HiHat" );
		sampler->SetTrackGain( TRACK_HIHAT, db20_to_ratio( -2 ) );

		sampler->SetTrackName( TRACK_SNARE, "Snare" );
		sampler->SetTrackGain( TRACK_SNARE, db20_to_ratio( -2 ) );

		sampler->SetTrackName( TRACK_KICK, "Bassdrum" );
		sampler->SetTrackGain( TRACK_KICK, db20_to_ratio( -2 ) );

		sampler->SetTrackName( TRACK_TOMS, "Tom Toms" );
		sampler->SetTrackGain( TRACK_TOMS, db20_to_ratio( -6 ) );

		sampler->SetMasterGain( db20_to_ratio( -6 ) );

		const int SAMPLE_CRASH = sampler->LoadSample( "crash.wav" );
		const int SAMPLE_HIHAT = sampler->LoadSample( "hihat.wav" );
		const int SAMPLE_SNARE = sampler->LoadSample( "snare.wav" );
		const int SAMPLE_KICK  = sampler->LoadSample( "kick.wav" );
		const int SAMPLE_TOM1  = sampler->LoadSample( "tom1.wav" );
		const int SAMPLE_TOM2  = sampler->LoadSample( "tom2.wav" );
		const int SAMPLE_TOM3  = sampler->LoadSample( "tom3.wav" );

		sampler->Print( );

		const double N1  = 1;
		const double N2  = 1 / 2.0;
		const double N4  = 1 / 4.0;
		const double N8  = 1 / 8.0;
		const double N16 = 1 / 16.0;
		const double N32 = 1 / 32.0;

		double t = 0;

		for( int i = 0; i < 100; i++ )
		{
			if( randmatch( 1 / 8.0 ) )
				sampler->AddPlaybackBySamplecount( SAMPLE_CRASH, TRACK_CRASH, int( t + 0 * N4 ) );

			double HH_ACCENT_GAIN = 0;
			double HH_NORMAL_GAIN = -12;

			// HiHat hinzuf�gen (On-beats akzentuiert)
			for( int j = 0; j < 8; j++ )
			{
				sampler->AddPlaybackBySamplecount( SAMPLE_HIHAT, TRACK_HIHAT, int( t + j * N8 ), false, db20_to_ratio( j % 3 == 0 ? HH_ACCENT_GAIN : HH_NORMAL_GAIN ) );
			}

			sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, int( t + 0 * N4 ) );
			if( !randmatch( 1 / 10.0 ) )
				// Snare auf 3
				sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, int( t + 4 * N8 ) );
			else if( randmatch( 1 / 2.0 ) )
			{
				// Snare auf 3, 3+ (zweiter etwas leiser)
				sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, int( t + 4 * N8 ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, int( t + 5 * N8 ), false, db20_to_ratio( -3 ) );
			}
			else
			{
				// Versetzte Snare
				sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, int( t + ( 4 + randdisp( ) ) * N8 ) );
			}


			// Zufall: 4+ auf der Basstrommel (etwas leiser)
			if( randmatch( 1 / 4.0 ) )
				sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, int( t + 7 * N8 ), false, db20_to_ratio( -3 ) );
			// Zufall: 1+ auf der Basstrommel (etwas leiser)
			if( randmatch( 1 / 6.0 ) )
				sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, int( t + 1 * N8 ), false, db20_to_ratio( -1 ) );


			if( randmatch( 1 / 2.0 ) )
				sampler->AddPlaybackBySamplecount( SAMPLE_TOM1 + irandx( 3 ), TRACK_TOMS, int( t + ( 2 + randdisp( ) ) * N8 ) );
			else
			{
				if( randmatch( 1 / 4.0 ) )
				{
					double u = irandx( 2 ) == 0 ? N8 : N16;
					double p = irandx( 10 );
					int j;
					for( j = 0; j < irandx( 80 ); j++ )
						sampler->AddPlaybackBySamplecount( SAMPLE_TOM1 + irandx( 3 ), TRACK_TOMS, int( t + ( j + p ) * u ), false, randgain( ) );

					if( randmatch( 0.75 ) )
						sampler->AddPlaybackBySamplecount( SAMPLE_CRASH, TRACK_CRASH, int( t + ( j + p + 1 ) * u ), false, randgain( ) );
				}
			}

			t += 1;
		}

		WriteFromDatasourceToFile( sampler, "out.wav", int( 60 * sampler->GetSampleRate( ) ), 1.0f, false, false );

		delete sampler;
	}
	catch( ITAException& e )
	{
		std::cerr << e << std::endl;
		delete sampler;
	}

	return 0;
}

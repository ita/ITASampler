// ITA includes
#include <ITAAsioInterface.h>
#include <ITAConstants.h>
#include <ITADataSourceUtils.h>
#include <ITAException.h>
#include <ITANumericUtils.h>
#include <ITASampleClock.h>
#include <ITASampleFrame.h>
#include <ITAStopWatch.h>
#include <ITAStreamProbe.h>

// ITA sampler
#include <ITASoundSamplePool.h>
#include <ITASoundSampler.h>

// ViSTA includes
#include <VistaBase/VistaTimeUtils.h>
#include <VistaInterProcComm/Concurrency/VistaThreadLoop.h>
#include <VistaTools/VistaRandomNumberGenerator.h>

// OpenDAFF
#include <DAFF.h>

// STL includes
#include <cassert>
#include <conio.h>
#include <iostream>
#include <time.h>


using namespace std;

class CRainMaker : public VistaThreadLoop
{
public:
	CRainMaker( )
	{
		dUpdateRate                = 1.0f;   // Hertz
		dRainDropDiameter          = 2.5e-3; // m
		dRainPrecipitation         = 5.0f;   // mm/hour
		dListenerGroundDistance    = 1.7;    // m
		dRainDropDistanceThreshold = 5;      // m

		m_iTimecodeCount = 0;
	}

	bool LoopBody( )
	{
		// Threshold underrun, no individual drops audible
		if( dRainDropDistanceThreshold - dListenerGroundDistance < 0 )
			return true;

		double dSampleRate = pDirectionalRainData->getSamplerate( );
		int iLength        = pDirectionalRainData->getFilterLength( );
		if( m_sfRainDropletContainer.length( ) != iLength )
			m_sfRainDropletContainer.init( 2, iLength, false );

		oStopWatch.start( );

		// Random rain process

		double dGroundCircleDiameter = sqrt( dRainDropDistanceThreshold * dRainDropDistanceThreshold - dListenerGroundDistance * dListenerGroundDistance );
		double dGroundCircleArea     = ITAConstants::PI_D * dGroundCircleDiameter * dGroundCircleDiameter / 4.0f;

		// double dRainDropVolume = ITAConstants::PI_D * dRainDropDiameter * dRainDropDiameter * dRainDropDiameter / 6.0f;
		double dRainTotalVolumePerSecond = dRainPrecipitation * dGroundCircleArea / 3.6f; // cubic meter

		assert( dRainDropDiameter > 0 );
		double dNumIndividualDropsPerSecond = dRainTotalVolumePerSecond / dRainDropDiameter / 1000;

		for( int i = 0; i < int( dNumIndividualDropsPerSecond ); i++ )
		{
			double dTimeCode                   = double( m_iTimecodeCount ) + m_oVRNG.GenerateDouble1( );
			double dRainDropDistanceOnGround   = dGroundCircleDiameter * m_oVRNG.GenerateDouble1( ) / 2.0f;
			double dAzimuthalAngleOnGround     = 360.0f * m_oVRNG.GenerateDouble1( );
			double dRainDropDistanceToLocation = sqrt( dRainDropDistanceOnGround * dRainDropDistanceOnGround + dListenerGroundDistance * dListenerGroundDistance );
			double dElevationToGroundLocation  = 180.0f / ITAConstants::PI_D * asin( dRainDropDistanceOnGround / dRainDropDistanceToLocation ) - 90;
			double dGain                       = m_oVRNG.GenerateDouble1( ) / dRainDropDistanceToLocation;


			int iIndex = -1;
			float f1   = float( dAzimuthalAngleOnGround );
			float f2   = float( dElevationToGroundLocation );
			pDirectionalRainData->getNearestNeighbour( DAFF_OBJECT_VIEW, f1, f2, iIndex );
			float* pfDestL = m_sfRainDropletContainer[0].data( );
			pDirectionalRainData->getFilterCoeffs( iIndex, 0, pfDestL, float( dGain ) );
			float* pfDestR = m_sfRainDropletContainer[1].data( );
			pDirectionalRainData->getFilterCoeffs( iIndex, 1, pfDestR, float( dGain ) );

			int iNewRainDropletSampleID = pSampler->CreateSample( pfDestL, pfDestR, iLength, dSampleRate );

			pSampler->AddPlaybackByTimecode( iNewRainDropletSampleID, iRainTrackID, dTimeCode, false );

			// pSampler->FreeSample( iNewRainDropletSampleID );
		}

		m_iTimecodeCount++;

		double dStop = oStopWatch.stop( );

		assert( dUpdateRate > 0 );
		double dTimeoutRemainderMilliseconds = 1000.0f * ( 1 / dUpdateRate - dStop );
		VistaTimeUtils::Sleep( int( dTimeoutRemainderMilliseconds ) );

		return true;
	};

	ITASoundSampler* pSampler;
	int iRainDropSampleID;
	int iRainTrackID;
	ITAStopWatch oStopWatch;
	double dUpdateRate;
	double dRainDropDiameter;
	double dRainPrecipitation;
	double dListenerGroundDistance;
	double dRainDropDistanceThreshold;
	DAFFContentIR* pDirectionalRainData;

private:
	int m_iTimecodeCount;
	VistaRandomNumberGenerator m_oVRNG;
	ITASampleFrame m_sfRainDropletContainer;
};

int main( int, char** )
{
	ITASoundSampler* pSampler = NULL;
	ITASoundSamplePool* pPool = NULL;
	CRainMaker* pRainMaker    = NULL;

	try
	{
		pPool    = ITASoundSamplePool::Create( 2, 44100 );
		pSampler = ITASoundSampler::Create( 2, 44100, 512, pPool );

		int iBinauralTrackID = pSampler->AddStereoTrack( );

		ITASampleFrame oRainDropSignal( "SingleWaterDrip2_short.wav" );
		int iBinauralSampleID =
		    pPool->CreateSample( oRainDropSignal[0].data( ), oRainDropSignal[0].data( ), oRainDropSignal.length( ), pPool->GetSampleRate( ), "RainDrop" );

		ITASampleFrame oRainNoiseSignal( "whitenoise.wav" );
		int iNoiseSampleID =
		    pPool->CreateSample( oRainNoiseSignal[0].data( ), oRainNoiseSignal[0].data( ), oRainNoiseSignal.length( ), pPool->GetSampleRate( ), "RainDrop" );

		pSampler->AddPlaybackByTimecode( iNoiseSampleID, iBinauralTrackID, 0, false, 0.03f );

		ITAStopWatch sw;
		sw.start( );
		DAFFReader* pReader = DAFFReader::create( );
		if( pReader->openFile( "rainyday.daff" ) != 0 )
			ITA_EXCEPT1( INVALID_PARAMETER, "File rainyday.daff not found" );

		DAFFContentIR* pDirectionalRain = dynamic_cast<DAFFContentIR*>( pReader->getContent( ) );

		if( pDirectionalRain == nullptr )
			ITA_EXCEPT1( INVALID_PARAMETER, "DAFF file not valid" );

		cout << "Loading directional rain took " << convertTimeToHumanReadableString( sw.stop( ) ) << endl;

		pRainMaker                       = new CRainMaker( );
		pRainMaker->pSampler             = pSampler;
		pRainMaker->iRainTrackID         = iBinauralTrackID;
		pRainMaker->iRainDropSampleID    = iBinauralSampleID;
		pRainMaker->pDirectionalRainData = pDirectionalRain;

		pRainMaker->Run( );

		ITAStreamProbe oOutputDumper( pSampler, "out.wav", ITAQuantization::ITA_FLOAT );

		ITAsioInitializeLibrary( );
		ITAsioInitializeDriver( "ASIO4ALL v2" );
		ITAsioCreateBuffers( 0, 2, 512 );
		ITAsioSetPlaybackDatasource( &oOutputDumper );
		ITAsioStart( );

		cout << "It's a rainy day. Hit '+' or '-' to control rain intensity. 'q' quits." << endl;

		int iGetCharacter;
		while( ( iGetCharacter = _getch( ) ) != 'q' )
		{
			if( iGetCharacter == '+' )
			{
				cout << "Increasing rain intensity" << endl;
			}
			else if( iGetCharacter == '-' )
			{
				cout << "Decreasing rain intensity" << endl;
			}
			else if( iGetCharacter == 'q' )
			{
				cout << "Stopping rain." << endl;
			}
			else
			{
				cout << "Unrecognized command " << char( iGetCharacter ) << endl;
			}
		}

		ITAsioStop( );

		pSampler->RemoveAllPlaybacks( );
		pRainMaker->StopGently( true );

		ITAsioFinalizeDriver( );
		ITAsioFinalizeLibrary( );

		delete pRainMaker;
		delete pSampler;
		delete pPool;
	}
	catch( ITAException& e )
	{
		delete pSampler;
		delete pPool;

		cerr << e << endl;
		return 255;
	}

	return 0;
}

﻿#include <ITAException.h>
#include <ITAAsioInterface.h>
#include <ITADatasourceUtils.h>
#include <ITANumericUtils.h>
#include <ITASoundSampler.h>
#include <conio.h>
#include <iostream>

int main( int argc, char* argv[] )
{
	if( argc != 2 )
	{
		fprintf( stderr, "Syntax: DrumsOnKeyboard ASIO-TREIBERNUMMER\n\n" );
		printf( "Verfügbare ASIO-Treiber:\n\n" );
		for( long i = 0; i < ITAsioGetNumDrivers( ); i++ )
			printf( "\t[%d] %s\n", i + 1, ITAsioGetDriverName( i ) );
		return 255;
	}

	long lASIODriver = atoi( argv[1] ) - 1;
	if( ( lASIODriver < 0 ) || ( lASIODriver >= ITAsioGetNumDrivers( ) ) )
	{
		fprintf( stderr, "Fehler: Ungültige ASIO-Treibernummer\n" );
		return 255;
	}

	if( ITAsioInitializeDriver( lASIODriver ) != ASE_OK )
	{
		fprintf( stderr, "Fehler: Konnte ASIO-Treiber nicht initialisieren\n" );
		return 255;
	}

	double dSamplerate;
	ITAsioGetSampleRate( &dSamplerate );

	long lDummy, lBuffersize;
	ITAsioGetBufferSize( &lDummy, &lDummy, &lDummy, &lBuffersize );
	// if (lBuffersize < 512) lBuffersize = 512;
	if( lBuffersize < 128 )
		lBuffersize = 128;
	printf( "Samplerate = %0.3f kHz, buffersize = %d samples\n", dSamplerate / 1000.0, lBuffersize );

	ITASoundSampler* sampler = NULL;

	try
	{
		// Sampler erzeugen
		sampler = ITASoundSampler::Create( 2, dSamplerate, lBuffersize );

		// Spuren anlegen und Samples laden
		const int TRACK_CRASH = sampler->AddTrack( sampler->GetNumberOfChannels( ) );
		const int TRACK_HIHAT = sampler->AddTrack( sampler->GetNumberOfChannels( ) );
		const int TRACK_SNARE = sampler->AddTrack( sampler->GetNumberOfChannels( ) );
		const int TRACK_KICK  = sampler->AddTrack( sampler->GetNumberOfChannels( ) );
		const int TRACK_TOMS  = sampler->AddTrack( sampler->GetNumberOfChannels( ) );

		sampler->SetTrackName( TRACK_CRASH, "Crash" );
		sampler->SetTrackGain( TRACK_CRASH, db20_to_ratio( -6 ) );

		sampler->SetTrackName( TRACK_HIHAT, "HiHat" );
		sampler->SetTrackGain( TRACK_HIHAT, db20_to_ratio( -2 ) );

		sampler->SetTrackName( TRACK_SNARE, "Snare" );
		sampler->SetTrackGain( TRACK_SNARE, db20_to_ratio( -4 ) );

		sampler->SetTrackName( TRACK_KICK, "Bassdrum" );
		sampler->SetTrackGain( TRACK_KICK, db20_to_ratio( -3 ) );

		sampler->SetTrackName( TRACK_TOMS, "Tom Toms" );
		sampler->SetTrackGain( TRACK_TOMS, db20_to_ratio( -6 ) );

		sampler->SetMasterGain( db20_to_ratio( -6 ) );

		const int SAMPLE_CRASH = sampler->LoadSample( "crash.wav" );
		const int SAMPLE_HIHAT = sampler->LoadSample( "hihat.wav" );
		const int SAMPLE_SNARE = sampler->LoadSample( "snare.wav" );
		const int SAMPLE_KICK  = sampler->LoadSample( "kick.wav" );
		const int SAMPLE_TOM1  = sampler->LoadSample( "tom1.wav" );
		const int SAMPLE_TOM2  = sampler->LoadSample( "tom2.wav" );
		const int SAMPLE_TOM3  = sampler->LoadSample( "tom3.wav" );

		sampler->Print( );

		if( ITAsioCreateBuffers( 0, sampler->GetNumberOfChannels( ), lBuffersize ) != ASE_OK )
		{
			delete sampler;
			fprintf( stderr, "Fehler: ASIOCreateBuffers fehlgeschlagen\n" );
			return 255;
		}

		ITAsioSetPlaybackDatasource( sampler );

		if( ITAsioStart( ) != ASE_OK )
		{
			delete sampler;
			fprintf( stderr, "Fehler: ASIOStart fehlgeschlagen\n" );
			return 255;
		}

		printf( "\nDas Schlagzeug steht bereit. Leg los!\nEin 9000er Recording Custom liegt unter Deinen Tasten!\n\n" );
		printf( "Tasten: b = Bassdrum, s = Snare, h = HiHat, c = Crash, 1-3 = Toms, i = Info, q = Ende\n" );
		printf( "Supertasten: r = Snare Wirbel (roll), f = Fill, t = Triolen Fill\n\n" );

		int c;
		while( ( c = _getch( ) ) != 'q' )
		{
			if( c == 'h' )
				sampler->AddPlaybackByTimecode( SAMPLE_HIHAT, TRACK_HIHAT, 0 );
			if( c == 's' )
				sampler->AddPlaybackByTimecode( SAMPLE_SNARE, TRACK_SNARE, 0 );
			if( c == 'b' )
				sampler->AddPlaybackByTimecode( SAMPLE_KICK, TRACK_KICK, 0 );
			if( c == 'c' )
				sampler->AddPlaybackByTimecode( SAMPLE_CRASH, TRACK_CRASH, 0 );
			if( c == '1' )
				sampler->AddPlaybackByTimecode( SAMPLE_TOM1, TRACK_TOMS, 0 );
			if( c == '2' )
				sampler->AddPlaybackByTimecode( SAMPLE_TOM2, TRACK_TOMS, 0 );
			if( c == '3' )
				sampler->AddPlaybackByTimecode( SAMPLE_TOM3, TRACK_TOMS, 0 );

			// Wirbel
			if( c == 'r' )
				for( int i = 0; i < 12; i++ )
					sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, (int)( -dSamplerate * 0.060 * i ),
					                                   ( i < 11 ? db20_to_ratio( -10 ) : db20_to_ratio( -4 ) ) );

			// Tom Fill
			if( c == 'f' )
			{
				double dt = dSamplerate * 0.100;
				int k     = 0;
				for( int i = 0; i < 4; i++ )
					sampler->AddPlaybackBySamplecount( SAMPLE_TOM1, TRACK_TOMS, (int)( -dt * k++ ), ( k % 4 ? 0.6 : 1 ) );
				for( int i = 0; i < 4; i++ )
					sampler->AddPlaybackBySamplecount( SAMPLE_TOM2, TRACK_TOMS, (int)( -dt * k++ ), ( k % 4 ? 0.6 : 1 ) );
				for( int i = 0; i < 4; i++ )
					sampler->AddPlaybackBySamplecount( SAMPLE_TOM3, TRACK_TOMS, (int)( -dt * k++ ), ( k % 4 ? 0.6 : 1 ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_CRASH, TRACK_CRASH, (int)( -dt * k ) );
			}

			// Tom Fill 2
			if( c == 't' )
			{
				double dt = dSamplerate * 0.075;
				int k     = 0;
				for( int i = 0; i < 6; i++ )
					if( i % 6 )
						sampler->AddPlaybackBySamplecount( SAMPLE_TOM1, TRACK_TOMS, (int)( -dt * k++ ), 0.6 );
					else
						sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k ) );
				for( int i = 0; i < 6; i++ )
					if( i % 6 )
						sampler->AddPlaybackBySamplecount( SAMPLE_TOM2, TRACK_TOMS, (int)( -dt * k++ ), 0.6 );
					else
						sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k ) );
				for( int i = 0; i < 6; i++ )
					if( i % 6 )
						sampler->AddPlaybackBySamplecount( SAMPLE_TOM3, TRACK_TOMS, (int)( -dt * k++ ), 0.6 );
					else
						sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k ) );

				sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_CRASH, TRACK_CRASH, (int)( -dt * k ) );
			}

			// Paradiddle
			if( c == 'p' )
			{
				double dt = dSamplerate * 0.150;
				int k     = 0;
				sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k++ ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, (int)( -dt * k++ ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k++ ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k++ ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, (int)( -dt * k++ ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_KICK, TRACK_KICK, (int)( -dt * k++ ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, (int)( -dt * k++ ) );
				sampler->AddPlaybackBySamplecount( SAMPLE_SNARE, TRACK_SNARE, (int)( -dt * k++ ) );
			}

			if( c == 'i' )
				sampler->Print( );
		}

		printf( "\nSchlagzeugspielen ist zuende. Hattest Du schon genug vom Recording Custom?\n\n" );

		ITAsioStop( );
		ITAsioDisposeBuffers( );

		delete sampler;
	}
	catch( ITAException& e )
	{
		std::cout << e << std::endl;
		delete sampler;
		return 255;
	}

	return 0;
}
